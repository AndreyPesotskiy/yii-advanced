<?php

namespace frontend\components;

use Yii;
class StringHelper
{
    private $limit;

    public function __construct()
    {
        $this->limit = Yii::$app->params['shortTextLimit'];
    }

    public function getShort($string, int $limit = null)
    {
        if ($limit === null) {
            $limit = $this->limit;
        }

        $words = explode(' ', $string);
        return implode(' ', array_slice($words, 0, $limit));
    }
}