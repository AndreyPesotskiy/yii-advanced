<?php

namespace frontend\controllers;

use frontend\models\News;
use yii\web\Controller;

class CountController extends Controller
{
    public function actionCount()
    {
        $countNews = News::getCountNews();
        return $this->render('index', [
            'count' => $countNews,
        ]);
    }
}