<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Test;

class TestController extends Controller
{

    public function actionIndex()
    {
        $limit = Yii::$app->params['maxNewsInList'];
        $list = Test::getListNews($limit);
        return $this->render('index', [
            'list' => $list,
        ]);
    }

}