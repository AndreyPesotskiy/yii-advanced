<?php

namespace frontend\models;

use Yii;
use frontend\components\StringHelper;
class Test
{

    public static function getListNews($limit)
    {
        $limit = (int)$limit;
        $sql = 'SELECT * FROM news LIMIT ' . $limit;
        $result = Yii::$app->db->createCommand($sql)->queryAll();

        if (!empty($result) && is_array($result)) {
            foreach ($result as &$item) {
                $item['content'] = Yii::$app->stringHelper->getShort($item['content']);
            }
        }

        return $result;
    }

}