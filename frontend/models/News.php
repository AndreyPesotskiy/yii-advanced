<?php

namespace frontend\models;

use Yii;
class News
{
    public static function getCountNews()
    {
        $sql = 'SELECT COUNT(*) as count FROM news';
        return Yii::$app->db->createCommand($sql)->queryScalar();
    }
}